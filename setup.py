import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="OuiTuto",
    version="0.0.3",
    author="Max",
    author_email="maxence.pellerin@epitech.eu",
    description="Type a short description",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/your_github_page(or any)",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)