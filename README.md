# Simple Project - Enter Name Here
 
Foobar is a Python library for dealing with word pluralization.

## Installation

Use [pip](https://pip.pypa.io/en/stable/) to install -Name-.

```bash
pip install -Name-
```

## Usage

```python
import - Name - 
```

## License
[MIT](https://choosealicense.com/licenses/mit/)