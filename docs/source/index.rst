.. OuiTuto documentation master file, created by
   sphinx-quickstart on Thu Jun 25 15:49:24 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OuiTuto's documentation!
===================================

.. toctree::
   :maxdepth: 2

   installation.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
