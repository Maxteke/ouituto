from selenium import webdriver

class Benchmarking():
  def __init__(self, browser):
    self.fd = None
    self.browser = browser

  def __del__(self):
    self.fd = None
    self.browser.close()

  def OuiTuto(self, url, link):
    self.browser.get(url)
    self.browser.find_element_by_link_text(link).click()